package particles;

public class Particle {
	public static final double K = 400D;
	double x, y, z, vx, vy;

	public Particle(double a, double b) {
		x = a;
		y = b;
		vx = 0;
		vy = 0;
	}

	public void update(double a, double b, Terrain t, double midx, double midy) {
		if (x == a && y == b) {
			double l = Math.sqrt((x - midx) * (x - midx) + (y - midy) * (y - midy)) * 100;
			vx += (x - midx) / l;
			vy += (y - midy) / l;
		} else {

			vx = (vx + a - this.x) / K;
			vy = (vy + b - this.y) / K;

		}

		x += vx;
		y += vy;
		z += (t.getHeightAt(x, y) - z) / 5;

	}
}
