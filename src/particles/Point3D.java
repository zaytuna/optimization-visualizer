package particles;


public class Point3D implements java.io.Serializable {
	private static final long serialVersionUID = 994272659202557850L;
	public double x, y, z;
	public int rgb;

	public Point3D(double a, double b, double c) {
		x = a;
		y = b;
		z = c;
	}

	public void set(double a, double b, double c) {
		x = a;
		y = b;
		z = c;
	}

	public String toString() {
		return "(" + Methods.getNumberWithin(x, .01) + "," + Methods.getNumberWithin(y, .01) + ","
				+ Methods.getNumberWithin(z, .01) + ")";
	}

	public void minus(Point3D other) {
		this.x -= other.x;
		this.y -= other.y;
		this.z -= other.z;
	}

	public double getHorizontalAngle() {
		return Math.atan2(y, x) + Math.PI / 2;
	}

	public void add(Point3D p) {
		x += p.x;
		y += p.y;
		z += p.z;
	}

	public static Point3D getSum(Point3D p1, Point3D p2) {
		return new Point3D(p1.x + p2.x, p2.y + p1.y, p2.z + p1.z);
	}

	public double getInclinationAngle() {
		return Math.acos(z / getDistanceFromOrigin());
	}

	public Point3D getLocation() {
		return this;
	}

	public double getDistanceFromOrigin() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	public double distanceFrom(Point3D p) {
		return Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y) + (z - p.z) * (z - p.z));
	}

	public Point3D scale(double scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return this;
	}

	public Point3D clone() {
		return new Point3D(x, y, z);
	}

	public boolean equals(Point3D other) {
		return (x == other.x) && (y == other.y) && (z == other.z);
	}

	public static Point3D subtract(Point3D v1, Point3D v2) {
		Point3D n = v1.clone();
		n.minus(v2);
		return n;
	}

	public static Point3D random() {
		return new Point3D(Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1);
	}
}