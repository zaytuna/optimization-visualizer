package particles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JPanel implements KeyListener, Runnable {
	private static final long serialVersionUID = -1404332382474580143L;
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	ArrayList<Particle> people = new ArrayList<Particle>();

	boolean bool = true;
	int leaderIndex = -1;

	Terrain t = Terrain.fractal(.15, 60, 7);
	LookAtCamera cam = new LookAtCamera(t.getWidth() / 2, t.getHeight() / 2, t.getHeightAt(t.getWidth() / 2,
			t.getHeight() / 2), Math.PI / 2, 2 * Math.PI / 3, t.getWidth() * 1.5);

	public Main() {
		setBackground(Color.BLACK);
		reset(Math.random(), Math.random(), 110, 1000);
		new Thread(this).start();

		System.out.println(cam.focus);
		addKeyListener(this);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		double hmin = t.minHeight(), hmax = t.maxHeight();

		for (int i = 0; i < t.getWidth(); i++)
			for (int j = 0; j < t.getHeight(); j++) {
				double prc = (t.getHeightAt(i, j) - hmin) / (hmax - hmin);
				g.setColor(Methods.colorMeld(Color.white, new Color(170, 0, 250), prc));
				g.fillRect(cam.getScreenX(t.getVertexAt(i, j), SCREEN.width),
						cam.getScreenY(t.getVertexAt(i, j), SCREEN.height), 1, 1);
			}

		for (int i = 0; i < people.size(); i++) {
			Point3D p = new Point3D(people.get(i).x, people.get(i).y, people.get(i).z);
			if (i == leaderIndex) {
				g.setColor(Color.CYAN);
				g.fillOval(cam.getScreenX(p, SCREEN.width), cam.getScreenY(p, SCREEN.height), 10, 10);
			} else {
				g.setColor(p.z == 0 ? Color.BLUE : Color.CYAN);
				g.fillOval(cam.getScreenX(p, SCREEN.width), cam.getScreenY(p, SCREEN.height), 3, 3);
			}
		}
	}

	public void reset(double x, double y, double r, int n) {
		t = Terrain.fractal(.15, 60, 7);
		cam.focus.z = t.avgHeight();
		cam.updatePosition();
		people.clear();
		for (int i = 0; i < n; i++) {
			double s = Math.random() * 2 * Math.PI;
			double real = r * Math.random() * Math.random();
			people.add(new Particle(x * t.getWidth() + Math.cos(s) * real, y * t.getHeight() + Math.sin(s) * real));
		}
	}

	public void update() {
		Particle leader = people.get(0);
		int index = 0;
		double lH = t.getHeightAt(people.get(0).x, people.get(0).y);
		double midx = 0, midy = 0;

		for (Particle p : people) {
			double h = t.getHeightAt(p.x, p.y);
			if (h > lH) {
				leader = p;
				lH = h;
				leaderIndex = index;
			}
			index++;

			midx += p.x;
			midy += p.y;
		}

		midx /= people.size();
		midy /= people.size();

		for (Particle p : people)
			p.update(leader.x, leader.y, t, midx, midy);
	}

	public void run() {
		int counter = 0;
		while (true) {
			try {
				Thread.sleep(20);
			} catch (Exception e) {
			}
			if (bool) {
				cam.incrementHorizontalAngle(0.005);
				cam.applyChanges();
			}

			repaint();
			update();
			if (counter++ % 1200 == 0)
				reset(Math.random(), Math.random(), 70, 1000);
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("LOL");
		frame.setSize(SCREEN);
		frame.setUndecorated(true);
		Main m = new Main();
		frame.add(m);
		frame.addKeyListener(m);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent evt) {
		if (evt.getKeyCode() == KeyEvent.VK_R)
			reset(Math.random(), Math.random(), 110, 1000);
		else if (evt.getKeyCode() == KeyEvent.VK_1)
			cam.changeRadius(3);
		else if (evt.getKeyCode() == KeyEvent.VK_2)
			cam.changeRadius(-3);
		else
			bool = !bool;

		cam.updatePosition();
		cam.applyChanges();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
}
